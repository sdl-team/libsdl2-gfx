libsdl2-gfx (1.0.4+dfsg-6) unstable; urgency=medium

  [ Simon McVittie ]
  * d/tests: Run Xvfb with -noreset to make the test more reliable.
    See #981201. While we're here, also let xvfb-run choose a display number
    instead of hard-coding :99.
  * Use debian/clean to clean up Docs/html/
  * d/clean: Clean up debian/build-tests/ (Closes: #1048719)
  * d/control, d/copyright: Update Homepage by following redirection

  [ Gianfranco Costamagna ]
  * Upload to sid

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 10 Dec 2024 22:38:41 +0100

libsdl2-gfx (1.0.4+dfsg-5) unstable; urgency=medium

  * Team upload

  [ Simon McVittie ]
  * d/rules: Remove compatibility with old -dbg package.
    This was removed in Debian 10.
  * Mark documentation as Multi-Arch: foreign
  * d/autoreconf: Run autoreconf on test/ subproject
  * d/rules: Explicitly delete libSDL2_gfx.la instead of ignoring it
  * d/p/test-Replace-undocumented-AC_CONFIG_AUX_DIRS-with-AC_CONF.patch,
    d/p/testgfx-Ensure-format-string-directives-are-not-interpret.patch,
    d/p/test-Match-filenames-case-sensitively.patch,
    d/p/test-Add-a-target-to-install-the-test-executables.patch,
    d/rules: Build and install manual tests.
    These are in libsdl2-gfx-dev for now, to avoid the NEW queue.
  * d/*.symbols: Add Build-Depends-Package
  * d/p/testframerate-Add-an-option-to-run-for-a-specified-number.patch,
    d/p/testrotozoom-Add-an-option-to-run-one-complete-cycle-and-.patch,
    d/p/testgfx-Add-an-option-to-run-one-complete-cycle-and-then-.patch:
    Add patches to make tests runnable noninteractively
  * d/tests: Add superficial autopkgtest coverage
  * d/copyright: Move Upstream-Name from d/upstream/metadata
  * d/control: Update standards version to 4.6.2 (no changes needed)
  * d/p/Don-t-include-full-paths-in-Doxygen-generated-documentati.patch:
    Add a patch to make the documentation more reproducible
  * Remove an unused Lintian override
  * d/source/lintian-overrides: Specify the source for Docs/html/*

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
    * Build-Depends: Drop versioned constraint on tar.

 -- Simon McVittie <smcv@debian.org>  Wed, 21 Jun 2023 15:46:40 +0100

libsdl2-gfx (1.0.4+dfsg-4) unstable; urgency=medium

  [ Simon McVittie ]
  * d/rules: Don't explicitly use --as-needed linker option.
    It's the default with bullseye toolchains.

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Archive, Name, Repository.

  [ Gianfranco Costamagna ]
  * Ack previously NMU, thanks
  * Also install SDL2_gfxPrimitives_font.h header file (Closes: #998195)
  * Bump std-version to 4.6.0
  * Bump compat level to 13

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 24 Nov 2021 11:50:33 +0100

libsdl2-gfx (1.0.4+dfsg-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 00:12:01 +0100

libsdl2-gfx (1.0.4+dfsg-3) unstable; urgency=medium

  * d/rules:
    - Override dh_auto_build-indep only, not -arch
    - Fix generating documentation.
      Since 1.0.1+dfsg-1 the doc was not actually shipped in the .deb
      packages, because it was generated in html/ instead of Docs/html, and
      dh_installdocs was failing to find it, but it was not a fatal error
      before switching to compat level v11:
        dh_installdocs: Cannot find (any matches for) "Docs/html/*" (tried in .)
    - Fix installing documentation in -doc.  It does not work when
      building both arch+indep, and it was not tested properly before
      creating the previous release due to a mistake invoking the command to
      build the package.
    - dh_install --fail-missing is deprecated and about to be removed, use
      dh_missing instead

 -- Manuel A. Fernandez Montecelo <mafm@debian.org>  Sat, 20 Oct 2018 01:22:52 +0200

libsdl2-gfx (1.0.4+dfsg-2) unstable; urgency=medium

  * Bump Policy Standards-Version to 4.2.1 (no changes needed)
  * d/watch: Set to version=4
  * d/control: Set "Rules-Requires-Root: no"
  * Switch to debhelper compat level v11
    - Keep installing documentation under libsdl2-gfx-doc, not -dev

 -- Manuel A. Fernandez Montecelo <mafm@debian.org>  Fri, 19 Oct 2018 23:30:07 +0200

libsdl2-gfx (1.0.4+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.4+dfsg
  * Bump copyright years
  * Bump std-version to 4.1.3
  * Switch to salsa and https URI
  * Refresh symbols
  * Bump compat level to 10, automatic autoreconf

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 22 Mar 2018 19:13:06 +0100

libsdl2-gfx (1.0.1+dfsg-6) unstable; urgency=medium

  * Bump Policy Standards-Version to 4.1.1
    - Stop using priority "extra" for the -doc package
  * Enable build hardening "+bindnow"

 -- Manuel A. Fernandez Montecelo <mafm@debian.org>  Sat, 28 Oct 2017 23:38:48 +0200

libsdl2-gfx (1.0.1+dfsg-5) unstable; urgency=medium

  * Bump Policy Standards-Version to 4.0.0 (no changes needed)
  * Update Vcs-* URLs
  * Switch to debhelper compat level v10
    - dh flags --parallel are not needed
    - autoreconf is invoked by default
  * Use automatic dbgsym packages, drop -dbg

 -- Manuel A. Fernandez Montecelo <mafm@debian.org>  Tue, 01 Aug 2017 01:20:42 +0200

libsdl2-gfx (1.0.1+dfsg-4) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Bump std-version
  * Make VCS fields in secure mode

  [ Reiner Herrmann ]
  * Make build reproducible (Closes: #828890)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 28 Jun 2016 22:10:11 +0200

libsdl2-gfx (1.0.1+dfsg-3) unstable; urgency=medium

  * fix watch file
  * fix copyright lintian issues.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sun, 24 Jan 2016 13:59:02 +0100

libsdl2-gfx (1.0.1+dfsg-2) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Update my uid
  * Upload to unstable

  [ Reiner Herrmann ]
  * Patch the examples tarball generation to make the build
    reproducible (Closes: #803576)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 31 Oct 2015 15:43:21 +0100

libsdl2-gfx (1.0.1+dfsg-1) unstable; urgency=medium

  * Repack without html in the source tree.
  * Do not symlink jquery from doxygen, this will result in a
    broken documentation. cfr: 736360.
  * Bump std-version, no changes required.
  * Update copyright file.
  * Create docs package with doxygen at build time.

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Wed, 05 Nov 2014 10:26:36 +0100

libsdl2-gfx (1.0.1-1) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Update watch file.

  [ Felix Geyer ]
  * New upstream release.
    - Includes a pkg-config file. (Closes: #750131)
  * Add jquery.js to debian/missing-sources/.
  * Stop creating the pointless libSDL2_gfx-1.0.so symlink, instead add a
    lintian override.

 -- Felix Geyer <fgeyer@debian.org>  Sat, 26 Jul 2014 23:08:42 +0200

libsdl2-gfx (1.0.0-2) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Enabling mmx only for i386 and amd64 architectures.

  [ Manuel A. Fernandez Montecelo ]
  * Link against math library
  * Modify how to fix the duplicated jquery.js

 -- Manuel A. Fernandez Montecelo <mafm@debian.org>  Mon, 03 Feb 2014 20:07:28 +0000

libsdl2-gfx (1.0.0-1) unstable; urgency=low

  * Initial Release (Closes: #735289)

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Tue, 14 Jan 2014 11:35:40 +0100
