#!/bin/sh
# Copyright 2019 Collabora Ltd.
# Copyright 2021 Simon McVittie
# SPDX-License-Identifier: Zlib
# (see "zlib/libpng" in debian/copyright)

set -eux

WORKDIR="$(mktemp -d)"
trap 'cd /; rm -fr "$WORKDIR"' 0 INT QUIT ABRT PIPE TERM

# Make sure the source tree is in a patches-applied state
dpkg-source --before-build .

if [ -n "${DEB_HOST_GNU_TYPE:-}" ]; then
    CROSS_COMPILE="$DEB_HOST_GNU_TYPE-"
else
    CROSS_COMPILE=
fi

cp test/*.bmp "$WORKDIR"
cp test/*.c "$WORKDIR"
mv "$WORKDIR/TestGfx.c" "$WORKDIR/testgfx.c"

cd "$WORKDIR"

for test in gfx framerate imagefilter rotozoom; do
    # shellcheck disable=SC2046
    "${CROSS_COMPILE}gcc" -o"test$test" "test${test}.c" \
        $("${CROSS_COMPILE}pkg-config" --cflags --libs SDL2_gfx) \
        -lSDL2_test -lm
done

set -- xvfb-run -a -s '-screen 0 1280x1024x24 -noreset'

"$@" ./testframerate --seconds 2
"$@" ./testgfx --once
"$@" ./testimagefilter
"$@" ./testrotozoom --once
